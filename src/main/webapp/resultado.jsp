<%@page import="root.examenAgain.dto.PalabraDto"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

     <%
          PalabraDto pal=(PalabraDto)request.getAttribute("palabraDto");
                
                %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Resultado consulta Oxford:</h1>
     
        <form name="form" action="DiccionarioController" method="GET">
        <h1>Palabra consultada:    <%= pal.getPalabra() %>
        <h1>Significado:   <%= pal.getSignificado() %> </h1>
        <button type="submit" name="accion" value="listar" class="btn btn-success">Ver lista de palabras</button>
        </form>  
    </body>
</html>