

<%@page import="java.util.Iterator"%>
<%@page import="root.examenagain.entity.Diccionario"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Diccionario> lista = (List<Diccionario>) request.getAttribute("lista");
    Iterator<Diccionario> itDiccionario = lista.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diccionario Oxford</title>
    </head>
    <body>
        <h1>Lista de gatitos</h1>
        <form name="form" action="DiccionarioController" method="GET">
            <table border="2">
                <th> ID </th>
                <th> Palabra </th>
                <th> Significado </th>
                    <% while (itDiccionario.hasNext()) {
                            Diccionario dic = itDiccionario.next();%>

                <tr>
                    <td> <%= dic.getId()%> </td>
                    <td> <%= dic.getPalabra()%> </td>
                    <td> <%= dic.getSignificado()%> </td>


                </tr>
                <%}%>
            </table>
            <button type="submit" name="accion" value="volver"> Volver </button>
        </form>
    </body>
</html>