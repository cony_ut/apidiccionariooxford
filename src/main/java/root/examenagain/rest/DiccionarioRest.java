/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.examenagain.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.examenAgain.dto.Palabra;
import root.examenAgain.dto.PalabraDto;

 @Path("diccionario")
public class DiccionarioRest {

    @GET
    @Path("{palabraElegida}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscar(@PathParam("palabraElegida") String palabraElegida) {
        
        
        Client cliente = ClientBuilder.newClient();
        WebTarget recurso = cliente.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + palabraElegida);
        Palabra palabra = recurso.request(MediaType.APPLICATION_JSON).header("app_id", "3cea3099").header("app_key", "27a2ae6a48954d63ce97bfe15c137335").get(Palabra.class);
        String def = palabra.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
        PalabraDto palabraDto = new PalabraDto();
        palabraDto.setPalabra(palabraElegida);
        palabraDto.setSignificado(def);
        
        return Response.ok(200).entity(palabraDto).build();
    }

}
