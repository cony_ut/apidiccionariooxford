/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.examenagain.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import root.examenAgain.dto.Palabra;
import root.examenAgain.dto.PalabraDto;
import root.examenagain.dao.DiccionarioJpaController;
import root.examenagain.entity.Diccionario;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@WebServlet(name = "DiccionarioController", urlPatterns = {"/DiccionarioController"})
public class DiccionarioController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DiccionarioController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DiccionarioController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String boton = request.getParameter("accion");
        if (boton.equals("volver")) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        if (boton.equals("listar")) {
            DiccionarioJpaController dao = new DiccionarioJpaController();
            List<Diccionario> lista = dao.findDiccionarioEntities();
            request.setAttribute("lista", lista);
            request.getRequestDispatcher("listar.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DiccionarioJpaController dao = new DiccionarioJpaController();
        Diccionario dic = new Diccionario();
        String palabra = request.getParameter("palabra");

        dic.setPalabra(palabra);
        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("https://examenapioxford.herokuapp.com/api/diccionario/" + palabra);
        PalabraDto palabraDto = myResource.request(MediaType.APPLICATION_JSON).get(PalabraDto.class);
        String def = palabraDto.getSignificado();
        dic.setSignificado(def);
        try {
            dao.create(dic);
        } catch (Exception ex) {
            Logger.getLogger(DiccionarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("palabraDto", palabraDto);
        request.getRequestDispatcher("resultado.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
