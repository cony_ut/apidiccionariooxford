

package root.examenAgain.dto;

public class PalabraDto {
    
    private String palabra;
    private String significado;

    
    public String getPalabra() {
        return palabra;
    }

   
    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    
    public String getSignificado() {
        return significado;
    }

    
    public void setSignificado(String significado) {
        this.significado = significado;
    }
    

}
