
package root.examenAgain.dto;

import java.util.List;

public class Palabra {


    public String getId() {
        return id;
    }

   
    public void setId(String id) {
        this.id = id;
    }

    
    public Metadata getMetadata() {
        return metadata;
    }

   
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

   
    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

  
    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
    
    private String id;
    private Metadata metadata;
    private List<Result> results;
    private String word;
    
    
}
