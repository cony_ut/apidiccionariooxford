package root.examenAgain.dto;

import java.util.List;

public class Entry {

    /**
     * @return the etymologies
     */
    public List<String> getEtymologies() {
        return etymologies;
    }

    /**
     * @param etymologies the etymologies to set
     */
    public void setEtymologies(List<String> etymologies) {
        this.etymologies = etymologies;
    }

    /**
     * @return the grammaticalFeatures
     */
    public List<GrammaticalFeature> getGrammaticalFeatures() {
        return grammaticalFeatures;
    }

    /**
     * @param grammaticalFeatures the grammaticalFeatures to set
     */
    public void setGrammaticalFeatures(List<GrammaticalFeature> grammaticalFeatures) {
        this.grammaticalFeatures = grammaticalFeatures;
    }

    /**
     * @return the senses
     */
    public List<Sens> getSenses() {
        return senses;
    }

    /**
     * @param senses the senses to set
     */
    public void setSenses(List<Sens> senses) {
        this.senses = senses;
    }
    private List<String> etymologies;
    private List<GrammaticalFeature> grammaticalFeatures;
    private List<Sens> senses; 
    
}
